# JCC-FinalProject
Jabar Coding Camp Candradimuka
Final Project Bootcamp React Native

- Nama	    : Hansen Feriandy .
- Email	    : hansenferiandy@gmail.com .
- Instagram  : fhansen17 .

You can access and view the project via expo in this link https://expo.dev/@hansfy/MAOEBOOKS

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.
clone repository
```html
$ NPM install
$ NPM start
```
---
### Login & Signup page
| Login | | Signup | | Invalid case |
| ------ | ------ | ------ | ------ | ------ |
| <img src='https://gitlab.com/miraclemaker17/jcc-finalproject/-/raw/main/assets/screenshots%20apps/login.jpeg' width='360px' height='720px'>  | | <img src='https://gitlab.com/miraclemaker17/jcc-finalproject/-/raw/main/assets/screenshots%20apps/signup.jpeg' width='360px' height='720px'> | | <img src='https://gitlab.com/miraclemaker17/jcc-finalproject/-/raw/main/assets/screenshots%20apps/invalid username or password.jpeg' width='360px' height='720px'> |

---
### User Profile page
| About Apps | | My List Book | | Add New Book |
| ------ | ------ | ------ | ------ | ------ |
| <img src='https://gitlab.com/miraclemaker17/jcc-finalproject/-/raw/main/assets/screenshots%20apps/about apps.jpeg' width='360px' height='720px'> |  | <img src='https://gitlab.com/miraclemaker17/jcc-finalproject/-/raw/main/assets/screenshots%20apps/my list book.jpeg' width='360px' height='720px'> | |<img src='https://gitlab.com/miraclemaker17/jcc-finalproject/-/raw/main/assets/screenshots%20apps/add new book.jpeg' width='360px' height='720px'> |

---
### Book Shop
| Shop | | Book Detail |
| ------ | ------ | ------ |
| <img src='https://gitlab.com/miraclemaker17/jcc-finalproject/-/raw/main/assets/screenshots%20apps/bookShop.jpeg' width='360px' height='720px'> |  | <img src='https://gitlab.com/miraclemaker17/jcc-finalproject/-/raw/main/assets/screenshots%20apps/bookDetail.jpeg' width='360px' height='720px'> |

---
### Messaging and Coin Bank
| Message | | Chat Box | | Coin Bank |
| ------ | ------ | ------ | ------ | ------ |
| <img src='https://gitlab.com/miraclemaker17/jcc-finalproject/-/raw/main/assets/screenshots%20apps/Messages.jpeg' width='360px' height='720px'> |  | <img src='https://gitlab.com/miraclemaker17/jcc-finalproject/-/raw/main/assets/screenshots%20apps/chatBox.jpeg' width='360px' height='720px'> | |<img src='https://gitlab.com/miraclemaker17/jcc-finalproject/-/raw/main/assets/screenshots%20apps/coinWallet.jpeg' width='360px' height='720px'> |

---
### Generate Apk 
```html 
$ expo build:android -t apk 
```
<img src='https://gitlab.com/miraclemaker17/jcc-finalproject/-/raw/main/assets/screenshots%20apps/build%20apk.jpeg' width='1000px' height='600px'>

---
### Thanks for paying attention ❤❤❤
<img src='https://gitlab.com/miraclemaker17/jcc-finalproject/-/raw/main/assets/screenshots%20apps/bonus.gif' alt='thanks' align='middle'/>

---
### My inspiration and credit to : 

- https://github.com/V98Ganz/eco-books
- https://github.com/cmitz/Mobile-Bookstore
