import axios from "axios";

const apiKey = "AIzaSyA82KyIRZzJeGLEs7QGQ6b9r70i8PlDjS4";

const request = axios.create({
  baseURL: "https://www.googleapis.com/books/v1/",
});

export const fetchBooks = (title, author) => {
  return request
    .get(`volumes?q=${title}+inauthor:${author}&key=${apiKey}`)
    .then(({ data }) => {
      if (data.totalItems === 0) {
        return Promise.reject("Please enter a valid book silly!");
      }
      return data.items[0].volumeInfo;
    });
};

// const replaceSpacesForQuery = (string) => {
//   return string.replace(/\s/g, '+')
// }
