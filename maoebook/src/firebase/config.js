import * as firebase from 'firebase';
import '@firebase/auth';
import '@firebase/firestore';

const firebaseConfig = {
  apiKey: "AIzaSyDWX6XcDPngQG2xJyoH6mw-PM4qBczTIXM",
  authDomain: "maoebook-b6dfd.firebaseapp.com",
  databaseURL: "https://maoebook-b6dfd-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "maoebook-b6dfd",
  storageBucket: "maoebook-b6dfd.appspot.com",
  messagingSenderId: "998915750466",
  appId: "1:998915750466:web:27679b54fdd8d6955c0bdb"
};

if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
}

export { firebase };