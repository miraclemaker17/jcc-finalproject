import React, { Component } from "react";
import {
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  ScrollView,
  ImageBackground,
} from "react-native";
import { firebase } from "../../firebase/config";
import styles from "./styles";

export default class RegistrationCoinInfo extends Component {
  state = {
    myBooks: [],
    isLoading: true,
    bookId: "",
  };

  componentDidMount = () => {
    firebase
      .firestore()
      .collection("users")
      .doc(this.props.route.params.userData.id)
      .collection("books")
      .get()
      .then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          const bookData = doc.data();
          const bookId = doc.id;
          return this.setState({
            myBooks: bookData,
            isLoading: false,
            bookId: bookId,
          });
        });
      });
  };

  handleBookCoin = (text) => {
    if (text > 0 && text < 6) {
      const book = this.state.myBooks;
      book.bookValue = text * 1;
      firebase
        .firestore()
        .collection("users")
        .doc(this.props.route.params.userData.id)
        .collection("books")
        .doc(this.state.bookId)
        .set(book);
    } else {
      alert("This is an invalid Coin value, please re-enter! (between 1-5)");
    }
  };

  nextPage = () => {
    this.props.navigation.navigate("MAOEBOOK", {
      user: this.props.route.params,
    });
  };

  render() {
    if (this.state.isLoading) {
      return <></>;
    }

    return (
      <ScrollView style={styles.scrollview}>
        <ImageBackground
          style={styles.image_background}
          source={{
            uri:
              "https://images.unsplash.com/photo-1515549832467-8783363e19b6?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=582&q=80",
          }}
        >
          <View style={styles.content_container}>
            <Image
              style={styles.eco_logo}
              source={require("../../img/icon.png")}
            ></Image>
            <Text style={styles.welcome_text}>Welcome to MAOEBOOK!</Text>
            <Text style={styles.eco_about}>
              MAOEBOOK is a book trading app where we encourage you to walk,
              read, and recycle! You can buy and sell your old books in our
              BookShop with Coins, which you can earn by walking. For every
              1000 steps you walk you earn a Coin! Once you have earned
              enough Coins you can purchase a book.
            </Text>
            <View style={styles.line_break}></View>
            <Text style={styles.eco_about}>
              You have added {this.state.myBooks.bookTitle} for sale, please add
              how many Coins you would like to sell your book for. The cost
              of your book must be between 1 to 5 Coins.
            </Text>
            <Image
              source={{ uri: this.state.myBooks.bookImage }}
              style={{ width: 295, height: 450, resizeMode: "contain" }}
            />
            <TextInput
              placeholderTextColor="#aaaaaa"
              placeholder="BookCoin Value..."
              onChangeText={this.handleBookCoin}
              underlineColorAndroid="transparent"
              autoCapitalize="none"
              style={styles.coin_amount_entry}
            />
            <TouchableOpacity
              style={styles.get_started_button}
              onPress={this.nextPage}
            >
              <Text style={styles.button_text}>Get started!</Text>
            </TouchableOpacity>
          </View>
        </ImageBackground>
      </ScrollView>
    );
  }
}
